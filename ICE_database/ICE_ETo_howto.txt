To update the ICE_ETO.csv with your City project :
1. Go the webpage : https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=117&id_rubrique=39
2. Click on "Sélection et téléchargement" at the bottom of the web page
3. Choose the city, click on the ID and download the data file 
4. Copy the data of "Evapotranspiration Potentielle (ETP Penman moyenne en mm)" into the csv

