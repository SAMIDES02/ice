# ICE is a free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License
# ICE - Version 1.0
# STEP 5 - Option 2

#### #### #### #### #### #### #### #### #### #### 
# ESTIMATION OF SURFACE TEMPERATURE OVER 24 HOURS #
#### #### #### #### #### #### #### #### #### #### 


#### #### #### #### #### #### #### #### #### #### 
#                IMPORT PACKAGES                #
#### #### #### #### #### #### #### #### #### #### 
from qgis.core import (
  QgsApplication,
  QgsDataSourceUri,
  QgsCategorizedSymbolRenderer,
  QgsClassificationRange,
  QgsPointXY,
  QgsProject,
  QgsExpression,
  QgsField,
  QgsFields,
  QgsFeature,
  QgsFeatureRequest,
  QgsFeatureRenderer,
  QgsGeometry,
  QgsGraduatedSymbolRenderer,
  QgsMarkerSymbol,
  QgsMessageLog,
  QgsRectangle,
  QgsRendererCategory,
  QgsRendererRange,
  QgsSymbol,
  QgsVectorDataProvider,
  QgsVectorLayer,
  QgsVectorFileWriter,
  QgsWkbTypes,
  QgsSpatialIndex,
  QgsVectorLayerUtils
)
from qgis.core.additions.edit import edit
import processing
from qgis.core import *
import qgis.utils
import numpy as np
import pandas as pd
from random import randrange
from qgis.utils import iface
import os
from scipy import optimize
import time
import csv

#### #### #### #### #### #### #### #### #### #### 
#                 RETRIEVE DATA                 #
#### #### #### #### #### #### #### #### #### #### 

# Retrieve mesh file (selected layer)
myfilepath= iface.activeLayer().dataProvider().dataSourceUri()
(myDirectory,nameFile) = os.path.split(myfilepath)

# Save it as a csv file
layer = iface.activeLayer()
rand = str((randrange(900)))
import_file_path = myDirectory + '/ICU_temporaire' + rand+ '.csv'
QgsVectorFileWriter.writeAsVectorFormat(layer,import_file_path, 'utf-8', layer.crs(), "CSV")

# Convert data to a dataframe
df = pd.read_csv (import_file_path, sep=',')

# Retrieve project data from the dataframe and convert into separate lists 

### GROUND PROPERTIES
# Data about ground material properties
ID_points= df['ID_p'].tolist() # unique id
alb = df['_alb'].tolist() # albedo (alpha) [0;1]
em = df['_em'].tolist() # emissivity (epsilon) [0;1]
ep= df['_ep'].tolist() # ground thickness [m]
Cv= df['_Cv'].tolist() # thermal capacity (Cv) [J/(m3K)]
lambd=df['_lambd'].tolist() # thermal conductivity (lambda) [W/(mK)]
kc=df['_kc'].tolist() # vegetation coefficient [0;1]

### SHADOWS 
# Shadows values from UMEP (=1 if shadowed, =0 if not)
o_6= df['6h_DN'].tolist()
o_7= df['7h_DN'].tolist()
o_8= df['8h_DN'].tolist()
o_9= df['9h_DN'].tolist()
o_10= df['10h_DN'].tolist()
o_11= df['11h_DN'].tolist()
o_12= df['12h_DN'].tolist()
o_13= df['13h_DN'].tolist()
o_14= df['14h_DN'].tolist()
o_15= df['15h_DN'].tolist()
o_16= df['16h_DN'].tolist()
o_17= df['17h_DN'].tolist()
o_18= df['18h_DN'].tolist()
o_19= df['19h_DN'].tolist()
o_20= df['20h_DN'].tolist()
o_21=[0]*(len(o_14))
o_22= [0]*(len(o_14))
o_23= [0]*(len(o_14))
o_24= [0]*(len(o_14))
o_1= [0]*(len(o_14))
o_2= [0]*(len(o_14))
o_3= [0]*(len(o_14))
o_4= [0]*(len(o_14))
o_5= [0]*(len(o_14))
shadow= [o_1,o_2,o_3,o_4,o_5,o_6,o_7,o_8,o_9,o_10,o_11,o_12,o_13,o_14,o_15,o_16,o_17,o_18,o_19,o_20,o_21,o_22,o_23,o_24]

### WEATHER DATA
hour= list(range(1, 25)) # 24 hours
# Retrieve weather data from csv file (24 hours of July 21st from .epw)
# layer = QgsProject.instance().mapLayersByName("weather")[0]
# weather_data = pd.DataFrame(list(zip([i.attributes()[layer.fields().indexFromName('Dry Bulb Temperature {C}')] for i in layer.getFeatures()],[i.attributes()[layer.fields().indexFromName('Global Horizontal Radiation {Wh/m2}')] for i in layer.getFeatures()],[i.attributes()[layer.fields().indexFromName('Liquid Precipitation Depth {mm}')] for i in layer.getFeatures()])), 
# columns=["temperature", "gh", "lpd"])

# Ask weather filepath to the user (epw file)
filepath = QFileDialog.getOpenFileName(None, 'Select weather file in epw - Choisissez le fichier météo au format epw')[0]
with open(filepath, newline='') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for i,row in enumerate(csvreader):
        if row[0].isdigit():
            break
first_row=i # Get first row of epw file
names=['Year', 'Month','Day', 'Hour','Minute',
'Data Source and Uncertainty Flags','Dry Bulb Temperature','Dew Point Temperature',
'Relative Humidity','Atmospheric Station Pressure','Extraterrestrial Horizontal Radiation',
'Extraterrestrial Direct Normal Radiation','Horizontal Infrared Radiation Intensity',
'Global Horizontal Radiation','Direct Normal Radiation','Diffuse Horizontal Radiation',
'Global Horizontal Illuminance','Direct Normal Illuminance','Diffuse Horizontal Illuminance',
'Zenith Luminance','Wind Direction','Wind Speed','Total Sky Cover',
'Opaque Sky Cover (used if Horizontal IR Intensity missing)','Visibility','Ceiling Height',
'Present Weather Observation','Present Weather Codes','Precipitable Water',
'Aerosol Optical Depth','Snow Depth','Days Since Last Snowfall','Albedo',
'Liquid Precipitation Depth','Liquid Precipitation Quantity']
weather_data=pd.read_csv(filepath, skiprows=first_row, header=None, names=names)
wd_save =weather_data.copy()
weather_data["Month"] = weather_data["Month"].astype(int)
weather_data = weather_data.loc[weather_data["Month"]==7]
weather_data = weather_data[["Month", "Day", "Hour", "Dry Bulb Temperature", "Global Horizontal Radiation", "Liquid Precipitation Depth"]]
weather_data.columns=["month", "day", "hour","temperature", "gh", "lpd"]
# Filter on 21st July
weather_data["day"] = weather_data["day"].astype(int)
weather_day = weather_data.loc[weather_data["day"]==21]

# Solar incident radiation
# Gh= [0,0,0,0,0,4.4,105.6,257.4,409.2,526.9,693,785.4,811.8,801.9,781,653.4,512.6,392.7,244.2,95.7,1.1,0,0,0]
Gh = weather_day["gh"].astype(float).values.tolist()
# Tair= [26.6,25.4,24.7,24.1,23.6,23.6,24.7,26.2,27.7,29.2,30.8,32.2,33.3,34.1,34.7,34.9,34.7,34.2,33.3,32.1,30.8,29.6,28.3,27.1] # Ambiant temperature
Tair = weather_day["temperature"].astype(float).values.tolist()
TairK = [x + 273.15 for x in Tair] # Convert °C to K
#weather_hourly = np.array([hour,Gh, Tair]).T
# Estimate sky temperature
Tskyb = round(np.mean(Tair),2) + 273.15 +1 ############# vérifier sure de ce 1 !!


# Evapotranspiration is based in the PenMan equation and obtained via monthly data from Meteo France
# ETo_city_july= 143.6 #mm/month
# Retrieve Evapotranspiration Potential data from csv file of the city (July)
layer = QgsProject.instance().mapLayersByName("ICE_ETo")[0]
ice_eto_data = pd.DataFrame(list(zip([i.attributes()[layer.fields().indexFromName('month')] for i in layer.getFeatures()],[i.attributes()[layer.fields().indexFromName('eto_project')] for i in layer.getFeatures()] )), 
columns=["month", "eto_project"])
ETo_city_july = ice_eto_data.loc[ice_eto_data["month"]=="7", "eto_project"].values[0]
print("ok")
# Transform into mm/day (we divide by 31 days in July), and multiply by the latent heat of vaporisation, divide again by 3600 to have it in seconds
ETo_units= (float(ETo_city_july)/31)*2260000/3600
# To normalize the ETP in the day (to avoid doing ETo/24 in 24h in the day) we use the following averaging values
ETo_norm= (0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.03,0.06,0.09,0.1,0.11,0.11,0.11,0.09,0.07,0.04,0.03,0.02,0.01,0.01,0.01)
ETo= [round(x * ETo_units,4) for x in ETo_norm] #ETo in W/m2.s

### CONSTANT VALUES
Tint= 308.15  #Temperature of the ground in K
hc=5 # Wind coefficient W.m-2.K-1
sigma = 5.57e-08 #Stefan - Boltzman constant in W.m-2.K-4
threshold = 0.01 # Calculation threshold, default = 1%

# Tests Stephanie
#default kc=1 for grass # only for testing 
#kc= [x*0 +1 for x in alb]



#### #### #### #### #### #### #### #### #### #### 
#                   FUNCTIONS                   #
#### #### #### #### #### #### #### #### #### ####

def params (Gh, alb, em, sigma, Tskyb, hc, TairK, lambd, ep, Tint, Cv, Tsurf0, m, h, shadow, ETo, kc):
 
    # We want to solve the equation for the balance of energy to get surface temperature
    # calc[m][h]= -Gh[h]*(1-alb[m]/100)+((em[m]/100)*sigma*((Tsurf[m][h]+273.15)**4-(Tskyb)**4))+hc*(Tsurf[m][h]-Tair[h])+(lambd[m]/ep[m])*(Tsurf[m][h]+273.15-Tint)+ Cv[m]*ep[m]*DTsdt+EVP
    # with DTsdt= (Tsurf-Tsurf0)/3600
    
    # Equation which can be simplified as A + BT + CT^4
    # This function returns the 3 parameters : A, B and C
   
    # Depending on :
    
    # Gh(solar incidence) and Tair (Temperature of the air at 10m) are array depending on the hour
     
    # Tint is a float and stands for the Temperature of the ground in K, which is consider constant 
    # Tskyb is a float and stands for the Temperature of the sky in K , which is consider constant 
     
    # alb (albedo), em (emissivity),lambd (thermique conductivity coef in W/m.K),ep (thickness in m), 
    # Cv (thermal volumetric capacity of concrete in J/m3.K) are arrays depending on the material/point
    
    # hc is a float and stands for the wind coef W.m-2.K-1

    # sigma is a float and stands for the Stefan - Boltzman constant in W.m-2.K-4
    
    # m int of materials/points
    # h int of hour of the day
    
    # x= Tsurf2 of the moment of calculation
    # Tsurf0 is the Tsurfi-1, so the Temperature of surface in the time i-1
    
    
    # Separate direct and diffuse solar incidence
    Ghdrt= [round(x * 0.8, 2) for x in Gh] # Direct solar incidence 
    Ghdif= [round(x * 0.2, 2) for x in Gh] # Diffuse solar incidence
    
    # Evaluate each part of the equation
    a0= -(Ghdrt[h]*shadow[h][m]*(1-alb[m])+ Ghdif[h])
    #a0= -Gh[h]*(1-alb[m]/100)
    c= (em[m])*sigma
    a1= c* -(Tskyb)**4 
    a2= -hc*TairK[h]
    b1= hc
    b2= (lambd[m]/ep[m])
    a3= -b2*Tint
    b3= Cv[m]*ep[m]/3600
    a4= -b3*(Tsurf0)
    a5= ETo[h]*kc[m]

    A= a0+a1+a2+a3+a4+a5
    B= (b1+ b2+ b3)
    C= c
    return A,B,C


def objective1(x,A,B,C):  
# Calculates the equation of the energy balance with the variables given
    return A+ B*x + C*((x)**4)




#### #### #### #### #### #### #### #### #### #### 
#                     MAIN                      #
#### #### #### #### #### #### #### #### #### ####

hours= 24
# Create empty table of 24 hours for each point
min_Tsurf= np.ones((len(alb), hours))
min_TsurfC= np.ones((len(alb), hours))
min_TsurfC2= np.ones((len(alb), hours))

start_time = time.time()

# Solve the equation at each point during 24 hours and fill in the tables
for m in range (len(alb)): # For each point
    Tsurf0 = 34 +273.15 # Initialize surface temperature
    fin= False
    count=0
    while fin==False: # Start iterations
        for h in range (hours): # Each hour
            
            # Calculate the different parameters
            A,B,C = params(Gh, alb, em, sigma, Tskyb, hc, TairK, lambd, ep, Tint, Cv, Tsurf0, m, h, shadow, ETo, kc)
            args= (A,B,C)
            
            # Solve equation A + BT + CT^4 = 0
            sol = optimize.root(objective1,[Tsurf0+2],args)
            
            # Save results
            min_Tsurf[m,h]= sol.x[0] # Fill in the table for the concerned point and hour 
            min_TsurfC[m,h]= (min_Tsurf[m,h] - 273.15) # Result in degrees
            min_TsurfC2[m,h]= round(min_Tsurf[m,h] - 273.15,2) # Round result in degrees
            
            T_previous = Tsurf0 # Save previous value
            Tsurf0 = min_Tsurf[m,h] # Update initial value T0
            
        count=count+1
        # print(abs(T_previous-Tsurf0)/Tsurf0)
        # Iterate at least 3 times, check convergence and stop after 20 times  
        if count>=3:
            if abs(T_previous-Tsurf0)/Tsurf0<=threshold:
                fin=True
            if count>= 20:
                print("Point number : "+ str(m)+ " at hour "+ str(h)+ "did not respect the threshold convergence value")
                fin =True
    
        # if count>=500:
            # fin=True
            # print('fr:boucle infinie pour h=',h,'m=',m)
            # print('infinite loop for h=',h,'m=',m)
                       



#### #### #### #### #### #### #### #### #### #### 
#                  SAVE DATA                    #
#### #### #### #### #### #### #### #### #### ####

# Create a dataframe with estimated surface temperature and save data to a csv file 
df= pd.DataFrame(data=min_TsurfC2,index=ID_points, columns=hour)
rand = str((randrange(100))) # random number
file_path = myDirectory+ '/Temp_surf' + rand+ '.csv'
df= df.reset_index()
df.to_csv(file_path, index=False)

# Create a layer with the temperatures 
uri= "file:///" + file_path + "?delimiter=,"
name_layer= 'temperature' #+ rand
csv = QgsVectorLayer(uri,name_layer, 'delimitedtext')

# Join layer with points data
shp=iface.activeLayer()
uri_shp= myDirectory + '/Temp_surf2_'+str (randrange(100))+'.shp'
result= processing.run('qgis:joinattributestable',{'INPUT': shp,
               'FIELD': 'ID_p',
               'INPUT_2': csv,
               'FIELD_2': 'index',
               'FIELDS_TO_COPY': ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'],
               'METHOD': 1,
               'DISCARD_NONMATCHING':False,
               'PREFIX':'_',
               'OUTPUT': uri_shp})
result['OUTPUT']
shp2= QgsVectorLayer(uri_shp,'temperature_final','ogr')

# Field calculator : calculate mean temperature and add layer to qgis project
uri_shp3= myDirectory + '/Temp_surf2_mean_'+str (randrange(100))+'.shp'
result= processing.run('qgis:fieldcalculator',{'INPUT': shp2,
            'FIELD_LENGTH': 10,
            'FIELD_NAME': 'T_mean',
            'FIELD_PRECISION': 2,
            'FIELD_TYPE': 0,
            'FORMULA': '(\"_1\"+\"_2\"+\"_3\"+\"_4\"+\"_5\"+\"_6\"+\"_7\"+\"_8\"+\"_9\"+\"_10\"+\"_11\"+\"_12\"+\"_13\"+\"_14\"+\"_15\"+\"_16\"+\"_17\"+\"_18\"+\"_19\"+\"_20\"+\"_21\"+\"_22\"+\"_23\"+\"_24\")/24',
            'NEW_FIELD': True,
            'OUTPUT': uri_shp3})
result['OUTPUT']
shp3= QgsVectorLayer(uri_shp3,'temperature_mean','ogr')
QgsProject.instance().addMapLayer(shp3)


"""
shp=iface.activeLayer()
QgsVectorFileWriter.writeAsVectorFormat(layer,file_path, 'utf-8', layer.crs(), "CSV")
# Set properties for the join

shpField='ID'
csvField='ID_points'
joinObject = QgsVectorLayerJoinInfo()
joinObject.joinLayerId = csv.id()
joinObject.joinFieldName = csvField
joinObject.targetFieldName = shpField
shp.addJoin(joinObject)

writer = QgsVectorFileWriter.writeAsVectorFormat(vlayer,"hoppla.shp","utf-8",None,"CSV")


res = processing.run("qgis:joinattributestable",shp,csv,"ID","ID_points",r'D:/DATA/Temp_suhhhrf2.csv')
layer = QgsVectorLayer(res['OUTPUT_LAYER'], "joined layer", "ogr")
QgsMapLayerRegistry.instance().addMapLayer(layer)
"""